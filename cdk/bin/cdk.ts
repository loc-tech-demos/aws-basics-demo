#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { CdkStack } from '../lib/cdk-stack';

const app = new cdk.App();

const vpcId = app.node.tryGetContext('vpcId');
const roleArn = app.node.tryGetContext('roleArn');
const securityGroupId = app.node.tryGetContext('securityGroupId');
const bucketName = app.node.tryGetContext('bucketName');
const keyName = app.node.tryGetContext('keyName');

new CdkStack(app, 'CdkStack', {
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT || process.env.AWS_ACCOUNT_ID, // Use default account from environment
    region: process.env.CDK_DEFAULT_REGION || process.env.AWS_DEFAULT_REGION,   // Use default region from environment
  },
  vpcId,
  roleArn,
  securityGroupId,
  bucketName,
  keyName,
});