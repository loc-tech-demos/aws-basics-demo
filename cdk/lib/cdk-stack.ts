import * as cdk from 'aws-cdk-lib';
import * as s3 from 'aws-cdk-lib/aws-s3';
import { Construct } from 'constructs';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as iam from 'aws-cdk-lib/aws-iam';

interface StackProps extends cdk.StackProps {
  vpcId: string;
  roleArn: string;
  securityGroupId: string;
  bucketName: string;
  keyName: string;
}

const appPort = 8080;

export class CdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props: StackProps) {
    super(scope, id, props);

    // Get the current region
    const region = cdk.Stack.of(this).region;

    // Create an S3 bucket
    const bucket = new s3.Bucket(this, props.bucketName, {
      versioned: true,
      // autoDeleteObjects: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY, // Not recommended for production
    });

    // Import the specified VPC
    const vpc = ec2.Vpc.fromLookup(this, 'ImportedVPC', {
      vpcId: props.vpcId,
    });

    // Import the specified IAM Role
    const role = iam.Role.fromRoleArn(this, 'ImportedRole', props.roleArn);

    // Import the specified Security Group
    const securityGroup = ec2.SecurityGroup.fromSecurityGroupId(this, 'ImportedSecurityGroup', props.securityGroupId);

    // AMI (Amazon Machine Image) for the EC2 instance
    const ami = ec2.MachineImage.latestAmazonLinux2023({
      cpuType: ec2.AmazonLinuxCpuType.ARM_64,
    });

    // User data script to install Git, Node.js, and run a Node.js application
    const userData = ec2.UserData.forLinux();
    userData.addCommands(
      // update the system
      'yum update -y',
      'yum install -y git nodejs',

      // install the code
      'cd /home/ec2-user',
      'git clone https://gitlab.com/loc-tech-demos/aws-basics-demo.git',

      // set up .env file
      'cd aws-basics-demo',
      'echo "" > .env',
      `echo BUCKET_NAME=${bucket.bucketName} >> .env`,
      `echo AWS_REGION=${region} >> .env`,
      `echo PORT=${appPort} >> .env`,

      // set up and run the program
      'npm ci',
      'npm install -g pm2',
      'pm2 start "npm start" --name webapp',
      'pm2 save', // Save the PM2 process list
      'pm2 startup systemd', // Generate the PM2 startup script
      'systemctl enable pm2-ec2-user' // Enable the PM2 startup script
    );

    // Create the EC2 instance
    const instance = new ec2.Instance(this, 'demo-instance', {
      vpc,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T4G, ec2.InstanceSize.NANO),
      machineImage: ami,
      role,
      securityGroup,
      keyPair: ec2.KeyPair.fromKeyPairName(this, 'KeyPair', props.keyName),
      userData: userData,
    });

    // Grant the EC2 instance read/write permissions on the bucket
    // bucket.grantReadWrite(instance.role);
  }
}
