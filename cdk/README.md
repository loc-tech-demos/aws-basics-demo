# Welcome to your CDK TypeScript project

This is a blank project for CDK development with TypeScript.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `npx cdk deploy`  deploy this stack to your default AWS account/region
* `npx cdk diff`    compare deployed stack with current state
* `npx cdk synth`   emits the synthesized CloudFormation template

## deploy / destroy template

We've exposed a number of parameters to configure this CDK script.

Check or add these items in your account, and configure it in a shell script:

```sh
#!/bin/sh
VPIC_ID=
ROLE_ARN=
SG_ID=
BUCKET_NAME=

# existing key name
KEY_NAME=

npx cdk deploy \
  --context vpcId=$VPC_ID \
  --context roleArn=$ROLE_ARN \
  --context securityGroupId=$SG_ID \
  --context bucketName=$BUCKET_NAME \
  --context keyName=$KEY_NAME \
```