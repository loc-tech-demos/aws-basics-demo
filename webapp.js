require('dotenv').config(); // Load environment variables from .env file
const path = require('path');
const express = require('express');
const { S3Client, PutObjectCommand, GetObjectCommand } = require('@aws-sdk/client-s3');
const app = express();
const port = process.env.PORT;

// Get your S3 bucket name and region from environment variables
const bucketName = process.env.BUCKET_NAME;
const region = process.env.AWS_REGION;

// Create an S3 client
const s3Client = new S3Client({ region });

// Middleware to parse JSON
app.use(express.json());

// Route to write data to S3
app.post('/avenger', async (req, res) => {
  const { key, data } = req.body;

  // construct the S3 PUT command parameters
  const params = {
    Bucket: bucketName,
    Key: key,
    Body: data,
  };

  // try to PUT a new object
  try {
    const command = new PutObjectCommand(params);
    await s3Client.send(command);

    // send the client a response
    res.send(`File uploaded successfully`).status(200);
    console.log('POST upload processed');
  } catch (err) {
    console.error(err);
    res.status(500).send('Error uploading file to S3');
  }
});

// Route to get data from S3
app.get('/avenger/:key', async (req, res) => {
  const key = req.params.key;

  // construct the S3 GET command
  const params = {
    Bucket: bucketName,
    Key: key,
  };

  try {
    const command = new GetObjectCommand(params);
    const data = await s3Client.send(command);

    // Set the correct content type for the response
    res.setHeader('Content-Type', 'text/plain');

    // Pipe the data from the S3 object to the response
    data.Body.pipe(res);
  } catch (err) {
    console.error(err);
    if (err.Code === 'NoSuchKey') {
      res.status(400).send('No Avenger Found');
    } else {
      res.status(500).send('Error downloading file from S3');
    }
  }
});

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Catch-all route to handle SPA routing (if using a front-end framework like React)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Open a HTTP server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
