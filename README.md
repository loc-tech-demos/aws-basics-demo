# AWS Basics Demo

## Demo steps

Assumes we have already introduced EC2, S3

1. Show the cloudcraft diagram [CloudCraft](https://app.cloudcraft.co/blueprint/657bc2c7-42ff-4483-a11b-b07382d45e30)
2. Talk about the architecture, what we plan to show: a functional end-to-end demo that allows users to enter and retrieve data
3. Key things to show:
  - Speed to create resources
  - Easiness to access storage (DX)
  - Simple performance benchmark
  - Quickly create, teardown
  - Cost transparency
4. Instantiate EC2
  - Talk about instance types, image type, key, SG, role, storage
  - Talk about speed to create
5. SSH into instance
6. sudo yum install git nodejs
7. Create S3 bucket
  - Talk about basic characteristics, such as storage type and encryption at rest
8. Configure and explain web app
9. Run application
10. Use Postman to upload, download data
11. Do it one more time with another piece of data
12. Show the files in the S3 bucket
13. Show the code again to explain how 5 lines of code can access storage
14. Show artillery code, and run it
15. Show and run CDK code
